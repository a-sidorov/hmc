package ru.nsu.homemoneycontrol.hmc.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;

@SpringBootTest(classes = HmcApplication.class)
@DisplayName("Интеграционные тесты на interactor")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Testcontainers
public class AuthorizationControllerTest {

    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Autowired
    private AuthorizationController authorizationController;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void registerTest() {
        ResponseEntity<UserDto> result = authorizationController.addUser(new UserDto(null, "testEmail@test.test", "testPassword"));
        Assertions.assertNotEquals(result.getStatusCode(), HttpStatus.CONFLICT);
        userRepository.deleteById(userRepository.findUserByEmail("testEmail@test.test").get().getId());
    }

    @Test
    @Transactional
    public void loginUser() {
        authorizationController.addUser(new UserDto(null, "testEmail@test.test", "testPassword"));
        ResponseEntity<String> result = authorizationController.login(new UserDto(null, "testEmail@test.test", "testPassword"));
        Assertions.assertNotEquals(result.getStatusCode(), HttpStatus.BAD_REQUEST);
        userRepository.deleteById(userRepository.findUserByEmail("testEmail@test.test").get().getId());
    }
}
