package ru.nsu.homemoneycontrol.hmc.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;
import ru.nsu.homemoneycontrol.hmc.utils.ResourceUtil;

@Testcontainers
@SpringBootTest(classes = {HmcApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@DirtiesContext
public class StatisticControllerTest {
    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @WithMockUser(username = "testStatistic@test.ru")
    @Test
    public void getStatisticTest() throws Exception {
//        //TODO тест нужно переделать, он несовместим с текущим деревом категорий
        var expectedBody = ResourceUtil.readFileToString("json/testStatisticResponse.json");
//
//        mockMvc.perform(get("/api/v1/statistic"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().json(expectedBody, true));
    }

}
