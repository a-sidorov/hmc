package ru.nsu.homemoneycontrol.hmc.interactor;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.dto.CreateReceiptDto;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.exception.InvalidReceiptUUID;
import ru.nsu.homemoneycontrol.hmc.exception.ReceiptNotFound;
import ru.nsu.homemoneycontrol.hmc.mapper.ReceiptMapper;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.User;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptMetainfoRepository;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@SpringBootTest(classes = HmcApplication.class)
@DisplayName("Интеграционные тесты на interactor чеков")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Testcontainers
public class ReceiptInteractorTest {

    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Autowired
    private UserInteractor userInteractor;

    @Autowired
    private ReceiptMetainfoRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReceiptInteractor receiptInteractor;


    @Autowired
    private ObjectMapper objectMapper;


    @AfterEach
    public void cleanUp() {
        repository.deleteAll();
        userRepository.deleteAll();
    }


    @Test
    @Transactional
    public void getOneReceiptTest() throws ReceiptNotFound {
        var user = createUser("test1");
        var check = createReceipt(user, CheckStatus.OK);

        var result = receiptInteractor.getReceiptMetaInfo(user.getId(), check.getReceiptId());
        Assertions.assertEquals(ReceiptMapper.receiptToDto(check), result.getBody());
    }

    @Test
    @Transactional
    public void getOneReceiptNotOwnTest() throws ReceiptNotFound {
        var user = createUser("test2");
        var check = createReceipt(user, CheckStatus.OK);
        Assertions.assertThrows(InvalidReceiptUUID.class, () -> {
            receiptInteractor.getReceiptMetaInfo(UUID.randomUUID(), check.getReceiptId());
        });
    }

    @Test
    @Transactional
    public void getOneReceiptNotOkStatusTest() throws ReceiptNotFound {
        var user = createUser("test3");
        var check = createReceipt(user, CheckStatus.WAITING_FOR_FNS);
        var res = receiptInteractor.getReceiptMetaInfo(user.getId(), check.getReceiptId());
        Assertions.assertEquals(CheckStatus.WAITING_FOR_FNS, res.getBody().getStatus());
        Assertions.assertNull(res.getBody().getTotal());
    }


    @Test
    @Transactional
    public void getAllReceiptTest() throws ReceiptNotFound {
        var user = createUser("test4");
        var check1 = createReceipt(user, CheckStatus.WAITING_FOR_FNS);
        var check2 = createReceipt(user, CheckStatus.ERROR);
        var check3 = createReceipt(user, CheckStatus.OK);

        var rightList = ReceiptMapper.receiptListToReceiptListDto(List.of(check1, check2, check3));
        var res = receiptInteractor.getReceiptShortList(user.getId());

        Assertions.assertNotNull(res.getBody());
        Assertions.assertEquals(res.getBody().size(), 3);
        Assertions.assertEquals(res.getBody(), rightList);

    }

    @Test
    @Transactional
    public void createReceipt() throws ReceiptNotFound, JsonProcessingException {
        var user = createUser("test4");
        String string = "{\n" +
                "  \"includeDate\": \"2021-11-17T17:08:06.968Z\",\n" +
                "  \"purchaseDate\": \"2021-11-17T17:08:06.968Z\",\n" +
                "  \"total\": 0,\n" +
                "  \"retailPlace\": \"string\",\n" +
                "  \"retailAddress\": \"string\",\n" +
                "  \"organizationName\": \"string\",\n" +
                "  \"organizationInn\": \"string\",\n" +
                "  \"sellerName\": \"string\",\n" +
                "  \"sellerInn\": \"string\",\n" +
                "  \"receiptPositions\": [\n" +
                "    {\n" +
                "      \"nomenclature\": \"string\",\n" +
                "      \"price\": 0,\n" +
                "      \"count\": 0,\n" +
                "      \"sum\": 0\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        var input = objectMapper.readValue(string, CreateReceiptDto.class);

        var out = receiptInteractor.createReceipt(user, input);

        Assertions.assertEquals(out.getBody().getQrCodeValue(), "Receipt created by user manually");
        Assertions.assertEquals(out.getBody().getOrganizationName(), input.getOrganizationName());
    }

    private ReceiptMetainfo createReceipt(User user, CheckStatus status) {
        ReceiptMetainfo metainfo = new ReceiptMetainfo();
        metainfo.setIncludeDate(Date.valueOf(LocalDate.now()));
        metainfo.setPurchaseDate(Date.valueOf(LocalDate.now()));
        metainfo.setAddedUser(user);
        metainfo.setQrCodeValue("QRCODE");
        metainfo.setTotal(BigDecimal.valueOf(100.0));
        metainfo.setOrganizationInn("INN");
        metainfo.setOrganizationName("org");
        metainfo.setRetailAddress("here");
        metainfo.setStatus(status);
        metainfo.setRetailPlace("here");
        metainfo.setSellerInn("INN");
        metainfo.setSellerName("name");

        return repository.save(metainfo);
    }

    private User createUser(String username) {
        UserDto testUserDto = new UserDto(null, username, "testPassword");
        userInteractor.addUser(testUserDto);
        return userRepository.findUserByEmail(username).get();
    }
}
