package ru.nsu.homemoneycontrol.hmc.interactor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.model.User;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;

@SpringBootTest(classes = HmcApplication.class)
@DisplayName("Интеграционные тесты на interactor")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Testcontainers
public class UserInteractorTest {

    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();
    /**
     * DTO юзера для тестов
     */
    private final UserDto testUserDto = new UserDto(null, "testEmail", "testPassword");
    @Autowired
    private UserInteractor userInteractor;
    @Autowired
    private UserRepository userRepository;

    @AfterEach
    public void deleteTestUser() {
        User testUser = userRepository.findUserByEmail(testUserDto.getEmail()).get();
        userRepository.deleteById(testUser.getId());
    }

    @Test
    @Transactional
    public void addUserTest() {
        UserDto result = userInteractor.addUser(testUserDto);
        Assertions.assertEquals(testUserDto, result);
    }

    @Test
    @Transactional
    public void getUserTest() {
        UserDto addedUser = userInteractor.addUser(testUserDto);
        UserDto result = userInteractor.getUserAsDTO(addedUser.getEmail());
        Assertions.assertEquals(addedUser, result);
    }
}
