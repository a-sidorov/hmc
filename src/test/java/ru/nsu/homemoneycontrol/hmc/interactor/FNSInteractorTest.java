package ru.nsu.homemoneycontrol.hmc.interactor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.interactor.feign.FNSClient;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

@SpringBootTest(classes = HmcApplication.class)
@DisplayName("Интеграционные тесты на interactor")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Testcontainers
public class FNSInteractorTest {

    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Autowired
    private FNSInteractor fnsInteractor;

    @MockBean
    FNSClient fnsClient;

    @BeforeEach
    public void init() {
        Mockito.when(fnsClient.sendQRCodeToFNS(any())).thenReturn(ResponseEntity.of(Optional.empty()));
    }

    @Test
    @WithMockUser(username = "testStatistic@test.ru")
    public void sendQRCodeTest() {
        UUID id = fnsInteractor.sendQRCodeOnValidate("testqrcode");
        Assertions.assertNotNull(id);
    }

    @Test
    @Transactional
    @WithMockUser(username = "testStatistic@test.ru")
    public void sendQRCodeAndCheckStatus() {
        UUID id = fnsInteractor.sendQRCodeOnValidate("testqrcode1");
        Assertions.assertDoesNotThrow(() -> fnsInteractor.checkQRCodeStatus(id));
    }

    @Test
    @Transactional
    @WithMockUser(username = "testStatistic@test.ru")
    public void refreshQRCodeStatus() {
        UUID id = fnsInteractor.sendQRCodeOnValidate("testqrcode1");
        Assertions.assertDoesNotThrow(() -> fnsInteractor.refreshQRCodeStatus(id));
    }

}
