package ru.nsu.homemoneycontrol.hmc.utils;

import lombok.SneakyThrows;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class ResourceUtil {

    @SneakyThrows
    public static String readFileToString(String path) {
        return FileCopyUtils.copyToString(getReader(path));
    }

    @SneakyThrows
    public static Reader getReader(String path) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(path);
        return new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8);
    }
}

