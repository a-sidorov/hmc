package ru.nsu.homemoneycontrol.hmc.utils;

import org.testcontainers.containers.PostgreSQLContainer;

public class CustomTestContainer extends PostgreSQLContainer<CustomTestContainer> {

    private static final String IMAGE_VERSION = "postgres:12.3";
    private static CustomTestContainer container;

    private CustomTestContainer() {
        super(IMAGE_VERSION);
    }

    public static CustomTestContainer getInstance() {
        if (container == null) {
            container = new CustomTestContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}

