package ru.nsu.homemoneycontrol.hmc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;

@Testcontainers
@SpringBootTest
class HmcApplicationTests {
    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Test
    void contextLoads() {
    }
}
