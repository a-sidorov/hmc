package ru.nsu.homemoneycontrol.hmc.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nsu.homemoneycontrol.hmc.HmcApplication;
import ru.nsu.homemoneycontrol.hmc.dto.GetCheckResponseFromFNSDto;
import ru.nsu.homemoneycontrol.hmc.mapper.fns.ReceiptMetainfoMapper;
import ru.nsu.homemoneycontrol.hmc.utils.CustomTestContainer;
import ru.nsu.homemoneycontrol.hmc.utils.ResourceUtil;

@SpringBootTest(classes = HmcApplication.class)
@Testcontainers
public class ReceiptMetainfoMapperTest {

    @Container
    private static final PostgreSQLContainer<CustomTestContainer> postgreSQLContainer = CustomTestContainer.getInstance();

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ReceiptMetainfoMapper receiptMetainfoMapper;

    @Test
    @SneakyThrows
    public void testMapping() {
        var body = ResourceUtil.readFileToString("/json/beforeMapping.json");
        var data = objectMapper.readValue(body, GetCheckResponseFromFNSDto.class);

        var mapped = receiptMetainfoMapper.map(data.getReceipt());

        var expected = ResourceUtil.readFileToString("/json/afterMapping.json");
        JSONAssert.assertEquals(expected, objectMapper.writeValueAsString(mapped), true);
    }
}
