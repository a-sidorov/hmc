insert into users(id, email, password)
VALUES ('5b374489-c7aa-40ee-a788-266cae92c473', 'testStatistic@test.ru', 'NoPassword');

insert into receipt_metainfo(receipt_id, added_user_id, purchase_date, status, qr_code_value)
values ('0a6704ec-04b0-4dc9-8d88-27924de1da7f', '5b374489-c7aa-40ee-a788-266cae92c473', '2021-08-21', 2, 'NoQr');

insert into categories(id, category, main_category_id)
VALUES ('0e9f5f5d-6e81-498f-9fab-2d97304f9f0b', 'Алкоголь', 1);

insert into categories(id, category, main_category_id)
VALUES ('6ed09017-40b1-4158-b3f8-fddb3862308b', 'Закуски', 1);

insert into receipt_position(id, receipt_id, nomenclature, price, count, sum, category_id)
VALUES ('8a9ac280-0d68-441c-9f05-19612f4e6763', '0a6704ec-04b0-4dc9-8d88-27924de1da7f', 'Пиво Жигулевское', 50, 2, 100,
        '0e9f5f5d-6e81-498f-9fab-2d97304f9f0b');

insert into receipt_position(id, receipt_id, nomenclature, price, count, sum, category_id)
VALUES ('d37265f9-2951-451e-b413-4beeb23463cb', '0a6704ec-04b0-4dc9-8d88-27924de1da7f', 'Водка Кнажеская', 1000, 1,
        1000, '0e9f5f5d-6e81-498f-9fab-2d97304f9f0b');

insert into receipt_position(id, receipt_id, nomenclature, price, count, sum, category_id)
VALUES ('e0830728-032f-4ede-8097-fbe4ca4f4a5e', '0a6704ec-04b0-4dc9-8d88-27924de1da7f', 'Бомж-Чипсы', 30, 1,
        30, '6ed09017-40b1-4158-b3f8-fddb3862308b');