package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Schema(name = "Ошибка", description = "DTO Статуса запроса")
public class StatusMessageDto {

    @Schema(description = "Внутреннее сообщение, пользователю не показывать", required = true)
    String message;
    @Schema(description = "Сообщеине для пользователя")
    String userMessage;

}

