package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Schema(description = "Сущность чека")
@Data
@AllArgsConstructor
@Builder
public class ReceiptListDto {
    private List<ShortReceiptDataDto> receiptList;
}
