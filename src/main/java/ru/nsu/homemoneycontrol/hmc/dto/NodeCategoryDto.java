package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Setter
@NoArgsConstructor
@Schema(description = "Модель листа категории")
public class NodeCategoryDto {

    @Schema(description = "Идентификатор категории", required = true)
    private UUID id;

    @Schema(description = "Сумма покупок", required = true)
    private BigDecimal totalSum = new BigDecimal("0.0");
    ;

    @Schema(description = "Название категории", required = true)
    private String category;
}
