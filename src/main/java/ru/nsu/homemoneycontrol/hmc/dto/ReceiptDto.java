package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Schema(description = "Сущность чека")
@Data
@AllArgsConstructor
@Builder
public class ReceiptDto {

    @Schema(description = "Дата добавления чека", required = true)
    private Date includeDate;

    @Schema(description = "Дата платежа")
    private Date purchaseDate;

    @Schema(description = "Значение QR кода", required = true)
    private String qrCodeValue;

    @Schema(description = "Сумма чека")
    private BigDecimal total;

    @Schema(description = "Место расчета")
    private String retailPlace;

    @Schema(description = "Адрес места расчета")
    private String retailAddress;

    @Schema(description = "Название организации")
    private String organizationName;

    @Schema(description = "ИНН организации")
    private String organizationInn;

    @Schema(description = "Продавец")
    private String sellerName;

    @Schema(description = "ИНН продавца")
    private String sellerInn;

    @Schema(description = "Статус ФНС", required = true)
    private CheckStatus status;

    @Schema(description = "Позиции чека")
    private List<PositionDto> receiptPositions;
}
