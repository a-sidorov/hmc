package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Data;

@Data
public class Document {
    private Receipt receipt;
}
