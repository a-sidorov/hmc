package ru.nsu.homemoneycontrol.hmc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Schema(description = "Сущность чека для отправки в сервис категоризации")
@Data
@AllArgsConstructor
@Builder
public class CategorizeServiceRequestDto {

    @Schema(description = "Место покупки", required = true)
    @JsonProperty("retail_place")
    private String retailPlace;

    @Schema(description = "Наименования товаров", required = true)
    @JsonProperty("names")
    private List<String> names;
}
