package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Schema(description = "Позиция в чеке")
@Data
@AllArgsConstructor
@Builder
public class PositionDto {

    @Schema(description = "Название товара", required = true)
    private String nomenclature;

    @Schema(description = "Цена товара", required = true)
    private BigDecimal price;

    @Schema(description = "Количество", required = true)
    private Integer count;

    @Schema(description = "Общая сумма", required = true)
    private BigDecimal sum;

    @Schema(description = "Категория")
    private CategoryDto category;
}
