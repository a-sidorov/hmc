package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Data;

@Data
public class Seller {

    private String name;
    private String inn;
}
