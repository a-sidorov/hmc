package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Data;

@Data
public class Organization {

    private String name;
    private String inn;
}
