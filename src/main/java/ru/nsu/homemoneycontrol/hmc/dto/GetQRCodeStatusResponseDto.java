package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;

import java.util.UUID;

@Schema(description = "Объект содержащий сообщение о статусе запроса")
@Data
@AllArgsConstructor
public class GetQRCodeStatusResponseDto {

    @Schema(description = "Идентификатор чека", required = true)
    private UUID id;

    @Schema(description = "Сообщение о статусе чека, мб пригодится", required = true)
    private String receipt;

    @Schema(description = "Статус чека", required = true)
    private CheckStatus status;
}
