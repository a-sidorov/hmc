package ru.nsu.homemoneycontrol.hmc.dto.predict;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;



@Schema(description = "Ответ микросервиса предсказаний")
@Data
@Setter
@Getter
@NoArgsConstructor
public class PredictsResponceDto {

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("data")
    private List<ResponcePositionDto> data;
}
