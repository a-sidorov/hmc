package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Data
@Setter
@NoArgsConstructor
@Schema(description = "Модель леса категории")
public class TreeCategoryDto {
    private List<RootCategoryDto> root;

    @Schema(description = "Сумма покупок", required = true)
    private BigDecimal totalSum = new BigDecimal("0.0");
    ;
}
