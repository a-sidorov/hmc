package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Data;

@Data
public class Ticket {

    private Document document;
}
