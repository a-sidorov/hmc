package ru.nsu.homemoneycontrol.hmc.dto.fns;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.dto.fns.converter.PriceConverter;

import java.math.BigDecimal;
import java.util.List;


@Data
public class Receipt {
    private List<Item> items;
    private String retailPlace;
    private String retailPlaceAddress;

    @JsonDeserialize(converter = PriceConverter.class)
    @JsonAlias("totalSum")
    private BigDecimal total;
}
