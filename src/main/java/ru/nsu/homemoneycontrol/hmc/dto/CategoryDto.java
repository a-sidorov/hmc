package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Модель категории")
public class CategoryDto {
    @Schema(description = "Идентификатор категории", required = true)
    private UUID id;

    @Schema(description = "Название категории", required = true)
    private String category;

    @Schema(description = "Название родительской категории", required = true)
    private MainCategoryDto mainCategory;
}
