package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "DTO валидирования QR кода чека")
public class SaveQRCodeDtoRequest {

    @Schema(description = "Строка с QR кодом", required = true)
    private String qrCode;

}
