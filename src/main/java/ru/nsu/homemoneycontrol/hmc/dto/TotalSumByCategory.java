package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.nsu.homemoneycontrol.hmc.model.Category;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Сумма покупок по категории")
public class TotalSumByCategory {
    @Schema(description = "Категория", required = true)
    private Category category;

    @Schema(description = "Сумма покупок", required = true)
    private BigDecimal totalSum;
}
