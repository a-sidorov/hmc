package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Schema(description = "Сущность пользователя")
@Data
@AllArgsConstructor
@Builder
public class UserDto {
    @EqualsAndHashCode.Exclude
    @Schema(description = "Идентификатор пользователя", required = false)
    private UUID id;

    @Schema(description = "Email пользователя", required = true)
    @Email(message = "Проверьте формат Email")
    @NotNull(message = "Не задан Email")
    private String email;

    @Schema(description = "Пароль пользователя", required = true)
    @NotNull(message = "Не задан пароль")
    @EqualsAndHashCode.Exclude
    @Size(min = 7, message = "Минимальная длина пароля 7")
    private String password;
}
