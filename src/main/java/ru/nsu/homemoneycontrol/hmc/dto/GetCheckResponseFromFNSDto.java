package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.dto.fns.FnsData;
import ru.nsu.homemoneycontrol.hmc.model.status.FNSCheckStatus;

import java.util.UUID;

@Schema(description = "Объект содержащий сообщение о статусе запроса для ФНС")
@Data
@AllArgsConstructor
public class GetCheckResponseFromFNSDto {

    @Schema(description = "Идентификатор чека", required = true)
    private UUID id;

    @Schema(description = "Сообщение о статусе чека, мб пригодится", required = true)
    private FnsData receipt;

    @Schema(description = "Статус чека (только для ФНС)", required = true)
    private FNSCheckStatus status;
}
