package ru.nsu.homemoneycontrol.hmc.dto.fns.converter;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.math.BigDecimal;

public class PriceConverter extends StdConverter<Integer, BigDecimal> {
    @Override
    public BigDecimal convert(Integer value) {
        return BigDecimal.valueOf(value, 2);
    }
}
