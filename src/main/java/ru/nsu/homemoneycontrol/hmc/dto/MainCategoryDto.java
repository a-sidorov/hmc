package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Модель общей категории")
public class MainCategoryDto {

    @Schema(description = "Идентификатор общей категории", required = true)
    private Long id;

    @Schema(description = "Название общей категории", required = true)
    private String mainCategoryName;
}
