package ru.nsu.homemoneycontrol.hmc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "Сущность категорий от сервиса категоризаций")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CategoryFromCategorizeServiceDto {

    @JsonProperty("category_id")
    private String categoryId;

    @JsonProperty("category_name")
    private String categoryName;

    @JsonProperty("name")
    private String name;
}
