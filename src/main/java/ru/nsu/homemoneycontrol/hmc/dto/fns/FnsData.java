package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Data;

@Data
public class FnsData {
    private Operation operation;
    private Ticket ticket;
    private Organization organization;
    private Seller seller;
}
