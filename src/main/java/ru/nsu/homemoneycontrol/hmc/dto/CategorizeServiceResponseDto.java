package ru.nsu.homemoneycontrol.hmc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Schema(description = "Сущность чека для отправки в сервис категоризации")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CategorizeServiceResponseDto {

    @Schema(description = "Место покупки", required = true)
    @JsonProperty("response")
    private List<CategoryFromCategorizeServiceDto> response;
}
