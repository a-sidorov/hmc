package ru.nsu.homemoneycontrol.hmc.dto.fns;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.dto.fns.converter.PriceConverter;

import java.math.BigDecimal;

@Data
public class Item {
    @JsonAlias("name")
    private String nomenclature;
    @JsonDeserialize(converter = PriceConverter.class)
    private BigDecimal price;

    @JsonAlias("quantity")
    private Integer count;

    @JsonDeserialize(converter = PriceConverter.class)
    private BigDecimal sum;
}
