package ru.nsu.homemoneycontrol.hmc.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Schema(description = "Сущность чека, созданная пользодателем")
@Data
@AllArgsConstructor
@Builder
public class CreateReceiptDto {
    @Schema(description = "Дата добавления чека", required = true)
    private Date includeDate;

    @Schema(description = "Дата платежа", required = true)
    private Date purchaseDate;

    @Schema(description = "Сумма чека")
    private BigDecimal total;

    @Schema(description = "Место расчета", required = true)
    private String retailPlace;

    @Schema(description = "Адрес места расчета", required = true)
    private String retailAddress;

    @Schema(description = "Название организации", required = true)
    private String organizationName;

    @Schema(description = "ИНН организации", required = true)
    private String organizationInn;

    @Schema(description = "Продавец")
    private String sellerName;

    @Schema(description = "ИНН продавца")
    private String sellerInn;

    @Schema(description = "Позиции чека")
    private List<PositionDto> receiptPositions;
}
