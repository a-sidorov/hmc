package ru.nsu.homemoneycontrol.hmc.dto.predict;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Schema(description = "Пара id категории - количество трат/товаров")
@Data
@Setter
@Getter
@AllArgsConstructor
public class SoldPointDto {

    @JsonProperty("cat_id")
    private String categoryId;

    @JsonProperty("sold")
    private List<BigDecimal> sold;

}
