package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Data
@Setter
@NoArgsConstructor
@Schema(description = "Модель дерева категории")
public class RootCategoryDto {

    @Schema(description = "Идентификатор общей категории", required = true)
    private Long id;

    @Schema(description = "Название общей категории", required = true)
    private String mainCategoryName;

    @Schema(description = "Сумма покупок", required = true)
    private BigDecimal totalSum = new BigDecimal("0.0");

    @Schema(description = "Подкатегории", required = true)
    private List<NodeCategoryDto> nodes;
}
