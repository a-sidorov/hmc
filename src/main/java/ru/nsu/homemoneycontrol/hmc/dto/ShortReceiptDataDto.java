package ru.nsu.homemoneycontrol.hmc.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

@Schema(description = "Сокращенная сущность чека")
@Data
@AllArgsConstructor
@Builder
public class ShortReceiptDataDto {

    @Schema(description = "Дата добавления", required = true)
    private Date includeDate;

    @Schema(description = "Дата платежа")
    private Date purchaseDate;

    @Schema(description = "Сумма чека")
    private BigDecimal total;

    @Schema(description = "Место продажи")
    private String retailPlace;

    @Schema(description = "Статус ФНС", required = true)
    private CheckStatus status;

    @Schema(description = "Количество позиций в чеке", required = true)
    private Integer nomenclatureCount;

    @Schema(description = "UUID пользователя", required = true)
    private UUID id;
}
