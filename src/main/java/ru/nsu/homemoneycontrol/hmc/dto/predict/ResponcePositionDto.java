package ru.nsu.homemoneycontrol.hmc.dto.predict;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;


@Schema(description = "Ответ микросервиса предсказаний - позиция")
@Data
@Setter
@Getter
@NoArgsConstructor
public class ResponcePositionDto {
    @JsonProperty("cat_id")
    private String categoryId;

    @JsonProperty("predict_four_weeks")
    private List<BigDecimal> predict;

    @JsonProperty("predict_month")
    private BigDecimal monthPredict;
}
