package ru.nsu.homemoneycontrol.hmc.dto.fns;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class FnsRequestDto {
    private UUID id;
    private String qr;
}
