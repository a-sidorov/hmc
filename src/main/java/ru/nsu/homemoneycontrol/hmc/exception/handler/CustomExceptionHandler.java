package ru.nsu.homemoneycontrol.hmc.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.exception.*;

import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

//    @ExceptionHandler(value = {
//
//    })
//    protected ResponseEntity<?> handleBadRequest(RuntimeException ex, WebRequest request) {
//
//        log.error("Handled bad request", ex);
//        return handleExceptionInternal(
//                ex,
//                StatusMessageDto.builder()
//                        .userMessage(ex.getMessage())
//                        .build(), new HttpHeaders(),
//                HttpStatus.BAD_REQUEST,
//                request);
//    }

    @ExceptionHandler(value = {
            BadReceiptStatusException.class
    })
    protected ResponseEntity<?> handleBadReceiptStatusException(@NonNull BadReceiptStatusException ex,
                                                                @NonNull WebRequest request) {
        return handleExceptionInternal(ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

    @ExceptionHandler(value = {
            FNSNegativeResponseException.class
    })
    protected ResponseEntity<?> handleFNSNegativeResponseException(@NonNull RuntimeException ex,
                                                                   @NonNull WebRequest request) {
        return handleExceptionInternal(ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

    @Override
    protected @NonNull
    ResponseEntity<Object> handleHttpMessageNotReadable(@NonNull HttpMessageNotReadableException ex,
                                                        @NonNull HttpHeaders headers, @NonNull HttpStatus status, @NonNull WebRequest request) {

        return handleExceptionInternal(ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(),
                headers,
                status,
                request);
    }

    @ExceptionHandler(value = {
            UserNotFound.class
    })
    protected ResponseEntity<?> handleNotFound(RuntimeException ex, WebRequest request) {
        log.error("Handled not found", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .userMessage(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = {
            UserAlreadyExists.class
    })
    protected ResponseEntity<?> handleConflict(RuntimeException ex, WebRequest request) {

        log.error("Handled not found", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .userMessage(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.CONFLICT,
                request);
    }

    @ExceptionHandler(value = {
            BadCredentialsException.class
    })
    protected ResponseEntity<?> handleBadCred(RuntimeException ex, WebRequest request) {

        log.error("Handled unauthorized", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message("Email или пароль не верны")
                        .userMessage("Email или пароль не верны")
                        .build(), new HttpHeaders(),
                HttpStatus.UNAUTHORIZED,
                request);
    }

    @ExceptionHandler(value = {
            InternalAuthenticationServiceException.class
    })
    protected ResponseEntity<?> handleUnauthorized(RuntimeException ex, WebRequest request) {

        log.error("Handled unauthorized", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.UNAUTHORIZED,
                request);
    }

    @ExceptionHandler(value = {
            InvalidReceiptUUID.class
    })
    protected ResponseEntity<?> handleForbidden(RuntimeException ex, WebRequest request) {

        log.error("Handled forbidden", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .userMessage(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.FORBIDDEN,
                request);
    }

    @ExceptionHandler(value = {
            RuntimeException.class
    })
    protected ResponseEntity<?> handleOthers(RuntimeException ex, WebRequest request) {

        log.error("Handled runtime error", ex);
        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<?> handleConstraintValidationException(ConstraintViolationException ex, WebRequest request) {

        log.error("Handled constraint violation", ex);

        var message = "Нарушены ограничения полей " + ex.getConstraintViolations().stream()
                .map(cv -> cv == null ? "null" : cv.getPropertyPath() + ": " + cv.getMessage())
                .collect(Collectors.joining(", "));

        var userMessage = ex.getConstraintViolations().stream()
                .map(cv -> cv == null ? "null" : cv.getMessage())
                .collect(Collectors.joining(", "));

        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(message)
                        .userMessage(userMessage)
                        .build(), new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {

        log.error("Handled method argument not valid", ex);

        var message = "Нарушены ограничения полей " + ex.getFieldErrors().stream()
                .map(cv -> cv == null ? "null" : cv.getField() + ": " + cv.getDefaultMessage())
                .collect(Collectors.joining(", "));

        var userMessage = ex.getFieldErrors().stream()
                .map(cv -> cv == null ? "null" : cv.getDefaultMessage())
                .collect(Collectors.joining(", "));

        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(message)
                        .userMessage(userMessage)
                        .build(), new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handleTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
        String name = ex.getName();
        String type = ex.getRequiredType().getSimpleName();
        Object value = ex.getValue();
        String message = String.format("'%s' should be a valid '%s' and '%s' isn't",
                name, type, value);

        log.error("Not valid arg" + message);

        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .build(), new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    @ExceptionHandler(value = {InvalidReceiptArgument.class})
    protected ResponseEntity<Object> handleInvalidReceiptArg(RuntimeException ex, WebRequest request) {

        return handleExceptionInternal(
                ex,
                StatusMessageDto.builder()
                        .message(ex.getMessage())
                        .userMessage("Проверьте заполнение обязательных полей или корректность введенных данных")
                        .build(), new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }
}

