package ru.nsu.homemoneycontrol.hmc.exception;

public class ReceiptNotFound extends RuntimeException {
    public ReceiptNotFound() {
        super("Чек не найден");
    }
}
