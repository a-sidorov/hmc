package ru.nsu.homemoneycontrol.hmc.exception;

/**
 * Класс исключение выбрасываемый в случае плохого ответа на запрос от ФНС сервиса
 */
public class BadFnsAnswerException extends RuntimeException {

    public BadFnsAnswerException(final String message) {
        super(message);
    }
}
