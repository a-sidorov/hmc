package ru.nsu.homemoneycontrol.hmc.exception;

public class FNSNegativeResponseException extends RuntimeException {

    public FNSNegativeResponseException(final String message) {
        super(message);
    }

}
