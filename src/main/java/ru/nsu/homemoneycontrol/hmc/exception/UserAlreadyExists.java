package ru.nsu.homemoneycontrol.hmc.exception;

public class UserAlreadyExists extends RuntimeException {
    public UserAlreadyExists() {
        super("Пользователь с заданным Email уже существует");
    }
}
