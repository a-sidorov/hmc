package ru.nsu.homemoneycontrol.hmc.exception;

public class InvalidReceiptArgument extends RuntimeException {
    public InvalidReceiptArgument(final String message) {
        super(message);
    }
}
