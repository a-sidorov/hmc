package ru.nsu.homemoneycontrol.hmc.exception;

public class InvalidReceiptUUID extends RuntimeException {
    public InvalidReceiptUUID(final String message) {
        super(message);
    }
}
