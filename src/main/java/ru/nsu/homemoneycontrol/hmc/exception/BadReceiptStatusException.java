package ru.nsu.homemoneycontrol.hmc.exception;

public class BadReceiptStatusException extends RuntimeException {
    public BadReceiptStatusException(final String message) {
        super(message);
    }
}
