package ru.nsu.homemoneycontrol.hmc.exception;

public class UserNotFound extends RuntimeException {
    public UserNotFound() {
        super("Пользователь не найден");
    }
}

