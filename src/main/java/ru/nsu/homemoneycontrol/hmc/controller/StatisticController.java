package ru.nsu.homemoneycontrol.hmc.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.dto.TotalSumByCategory;
import ru.nsu.homemoneycontrol.hmc.dto.TreeCategoryDto;
import ru.nsu.homemoneycontrol.hmc.interactor.StatisticInteractor;

import java.sql.Date;
import java.util.List;


@RestController
@RequiredArgsConstructor
@Tag(name = "[ANDROID] Контроллер статистики", description = "Позволяет получить статистику")
public class StatisticController {
    private final StatisticInteractor statisticInteractor;


    @GetMapping("/api/v1/statistic")
    @Operation(description = "Получить всю статистику по категориям за заданный промежуток времени")
    @ApiResponses(value = {
            @ApiResponse(description = "Успешно", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TreeCategoryDto.class)))),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = StatusMessageDto.class))),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    ResponseEntity<TreeCategoryDto> getStatistic(
            @Parameter(description = "Начало периода", schema = @Schema(format = "date", example = "2021-11-10")) @RequestParam(required = false) Date startPeriodDate,
            @Parameter(description = "Конец периода", schema = @Schema(format = "date", example = "2021-11-10")) @RequestParam(required = false) Date endPeriodDate) {
        return ResponseEntity.ok(statisticInteractor.getStatistic(startPeriodDate, endPeriodDate));
    }




    @GetMapping("/api/v1/tree")
    @Operation(description = "Получить полное дерево категорий без какой либо дополнительной информации")
    @ApiResponses(value = {
            @ApiResponse(description = "Успешно", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = TreeCategoryDto.class)))),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    ResponseEntity<TreeCategoryDto> getCategoryTree() {
        return ResponseEntity.ok(statisticInteractor.getTree());
    }
}
