package ru.nsu.homemoneycontrol.hmc.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.homemoneycontrol.hmc.dto.GetCheckResponseFromFNSDto;
import ru.nsu.homemoneycontrol.hmc.interactor.FNSResponseInteractor;

@RestController
@RequestMapping("/api/v1/fns")
@Tag(name = "[FNS] Котроллер ответов ФНС")
@AllArgsConstructor
public class FNSResponseController {

    private final FNSResponseInteractor fnsResponseInteractor;

    @PostMapping("/qr-code-response")
    @Operation(summary = "Ответ от ФНС на валидацию QR кода.")
    public ResponseEntity<?> fnsResponseForQRCode(@RequestBody final GetCheckResponseFromFNSDto requestEntity) {
        return fnsResponseInteractor.saveFnsResponse(requestEntity);
    }

}
