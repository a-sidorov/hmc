package ru.nsu.homemoneycontrol.hmc.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.homemoneycontrol.hmc.dto.GetQRCodeStatusResponseDto;
import ru.nsu.homemoneycontrol.hmc.dto.SaveQRCodeDtoRequest;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.interactor.FNSInteractor;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/")
@Tag(name = "[ANDROID] Контроллер управления добавления чека и отправки в ФНС")
@AllArgsConstructor
public class FNSController {

    private final FNSInteractor fnsInteractor;

    @ApiResponses(value = {
            @ApiResponse(description = "Если чек найден", responseCode = "200", content = @Content(mediaType = MediaType.TEXT_PLAIN_VALUE, schema = @Schema(implementation = UUID.class))),
            @ApiResponse(description = "Если чек не принадлежит пользователю", responseCode = "403", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = StatusMessageDto.class))),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @Operation(summary = "Загрузить чек")
    @PostMapping("/qrcode-validate")
    public ResponseEntity<UUID> sendQRCodeOnValidate(@RequestBody final SaveQRCodeDtoRequest saveQRCodeDtoRequest) {
        return ResponseEntity.ok(fnsInteractor.sendQRCodeOnValidate(saveQRCodeDtoRequest.getQrCode()));
    }

    @ApiResponses(value = {
            @ApiResponse(description = "Если чек найден", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = GetQRCodeStatusResponseDto.class))),
            @ApiResponse(description = "Если чек не принадлежит пользователю", responseCode = "403", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = StatusMessageDto.class))),
            @ApiResponse(description = "Если чек не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })

    @Operation(summary = "Получить статус чека")
    @GetMapping(value = "/qrcode-validate/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetQRCodeStatusResponseDto> checkQRCodeStatus(@PathVariable("id") final UUID uuid) {
        return ResponseEntity.ok(fnsInteractor.checkQRCodeStatus(uuid));
    }

    @ApiResponses(value = {
            @ApiResponse(description = "Если чек найден", responseCode = "200", content = @Content(schema = @Schema(implementation = UUID.class), mediaType = MediaType.TEXT_PLAIN_VALUE)),
            @ApiResponse(description = "Если чек не принадлежит пользователю", responseCode = "403", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = StatusMessageDto.class))),
            @ApiResponse(description = "Если чек не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })

    @Operation(summary = "Повторить запрос в ФНС")
    @GetMapping(value = "/qrcode-retry")
    public void refreshQRCodeStatus(@RequestParam("id") final UUID uuid) {
        fnsInteractor.refreshQRCodeStatus(uuid);
    }
}
