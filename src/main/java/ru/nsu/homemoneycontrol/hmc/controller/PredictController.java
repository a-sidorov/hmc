package ru.nsu.homemoneycontrol.hmc.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.dto.TreeCategoryDto;
import ru.nsu.homemoneycontrol.hmc.dto.predict.PredictsResponceDto;
import ru.nsu.homemoneycontrol.hmc.interactor.PredictInteractor;
import ru.nsu.homemoneycontrol.hmc.interactor.StatisticInteractor;

import java.sql.Date;

@RestController
@RequiredArgsConstructor
@Tag(name = "[ANDROID] Контроллер предсказаний", description = "Позволяет получить предсказание по тратам/количеству товаров")
public class PredictController {

    private final PredictInteractor predictInteractor;

    @GetMapping("/api/v1/predict_item")
    @Operation(description = "Получить предсказание количества номенклатур (товаров), которые будут приобретены на следующей неделе в каждой из 29 категорий")
    @ApiResponses(value = {
            @ApiResponse(description = "Успешно", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = PredictsResponceDto.class)))),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    ResponseEntity<PredictsResponceDto> getItemPrediction() {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(predictInteractor.predictItems(userName));
    }


    @GetMapping("/api/v1/predict_sold")
    @Operation(description = "Получить предсказание сумм, которые будут потрачены на следующей неделе в каждой из 29 категорий")
    @ApiResponses(value = {
            @ApiResponse(description = "Успешно", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = PredictsResponceDto.class)))),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    ResponseEntity<PredictsResponceDto> getSoldPrediction() {
        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(predictInteractor.predictSold(userName));
    }
}
