package ru.nsu.homemoneycontrol.hmc.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.nsu.homemoneycontrol.hmc.dto.CreateReceiptDto;
import ru.nsu.homemoneycontrol.hmc.dto.ReceiptDto;
import ru.nsu.homemoneycontrol.hmc.dto.ShortReceiptDataDto;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.interactor.ReceiptInteractor;
import ru.nsu.homemoneycontrol.hmc.interactor.UserInteractor;
import ru.nsu.homemoneycontrol.hmc.repository.CategoryRepository;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/")
@AllArgsConstructor
@Tag(name = "[ANDROID] Контроллер чеков", description = "Для работы с чеками")
public class ReceiptController {


    private final ReceiptInteractor receiptInteractor;
    private final UserInteractor userInteractor;

    @Operation(summary = "Получить  чек по id")
    @GetMapping(value = "/one-receipt/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(description = "Если чек найден", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ReceiptDto.class))),
            @ApiResponse(description = "Если чек не принадлежит пользователю", responseCode = "403", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = StatusMessageDto.class))),
            @ApiResponse(description = "Если чек не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    public ResponseEntity<ReceiptDto> getOneReceipt(@PathVariable final UUID id) {

        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return receiptInteractor.getReceiptMetaInfo(userInteractor.getUserUUID(userName), id);
    }


    @Operation(summary = "Получить  все чеки пользователя")
    @ApiResponses(value = {
            @ApiResponse(description = "Если все ок", responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = ShortReceiptDataDto.class)))),
            @ApiResponse(description = "Если пользователь не найден", responseCode = "404", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @GetMapping(value = "/all-receipts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ShortReceiptDataDto>> getAllReceipt() {

        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return receiptInteractor.getReceiptShortList(userInteractor.getUserUUID(userName));
    }


    @Operation(summary = "Создать чек вручную")
    @ApiResponses(value = {
            @ApiResponse(description = "Если все ок", responseCode = "200", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Некорректные данные или не заполнены обязательные поля", responseCode = "400", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    @PostMapping(value = "/create-receipt", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReceiptDto> createReceipt(
            @Parameter(description = "Сущность чека", content = @Content(schema = @Schema(implementation = CreateReceiptDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE), required = true) @RequestBody @Validated CreateReceiptDto requestEntity) {

        var userName = SecurityContextHolder.getContext().getAuthentication().getName();
        return receiptInteractor.createReceipt(userInteractor.getUser(userName), requestEntity);
    }
}
