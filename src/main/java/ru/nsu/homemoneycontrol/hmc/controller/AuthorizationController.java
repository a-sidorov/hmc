package ru.nsu.homemoneycontrol.hmc.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.homemoneycontrol.hmc.authorization.security.SecurityConfig;
import ru.nsu.homemoneycontrol.hmc.dto.StatusMessageDto;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.interactor.AuthorizationInteractor;

/**
 * Контроллер для авторизации пользователя.
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@Tag(name = "[ANDROID] Контроллер авторизации", description = "Позволяет пользователю зарегитрироваться и авторизоваться")
public class AuthorizationController {

    private final AuthorizationInteractor authorizationInteractor;

    @Operation(summary = "Регистрация пользователя")
    @PostMapping(SecurityConfig.REGISTER_ENDPOINT)
    @ApiResponses(value = {
            @ApiResponse(description = "Информация о пользователе", responseCode = "200", content = @Content(schema = @Schema(implementation = UserDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "В если пользователь с таким Email уже существует", responseCode = "409", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))

    })
    public ResponseEntity<UserDto> addUser(
            @RequestBody
            @Parameter(description = "DTO пользователя", required = true) @Validated UserDto userDto
    ) {
        return authorizationInteractor.registerUser(userDto);
    }

    @Operation(summary = "Авторизация пользователя")
    @PostMapping(value = SecurityConfig.LOGIN_ENDPOINT, produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiResponses(value = {
            @ApiResponse(description = "Токен авторизации", responseCode = "200", content = @Content(mediaType = MediaType.ALL_VALUE, schema = @Schema(implementation = String.class))),
            @ApiResponse(description = "В случае ошибки логина", responseCode = "401", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
            @ApiResponse(description = "Непредвиденная жесть", responseCode = "500", content = @Content(schema = @Schema(implementation = StatusMessageDto.class), mediaType = MediaType.APPLICATION_JSON_VALUE))
    })
    public ResponseEntity<String> login(@RequestBody
                                            @Validated UserDto userDto
    ) {
        return authorizationInteractor.login(userDto);
    }

}
