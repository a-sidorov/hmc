package ru.nsu.homemoneycontrol.hmc.interactor.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.nsu.homemoneycontrol.hmc.dto.fns.FnsRequestDto;

@FeignClient(name = "FNSClient", url = "${fns-service.url}")
public interface FNSClient {

    @PostMapping("/HMC/qr")
    ResponseEntity<?> sendQRCodeToFNS(@RequestBody FnsRequestDto qrCode);

}
