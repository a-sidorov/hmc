package ru.nsu.homemoneycontrol.hmc.interactor;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.dto.*;
import ru.nsu.homemoneycontrol.hmc.exception.InvalidReceiptArgument;
import ru.nsu.homemoneycontrol.hmc.exception.InvalidReceiptUUID;
import ru.nsu.homemoneycontrol.hmc.exception.ReceiptNotFound;
import ru.nsu.homemoneycontrol.hmc.interactor.feign.FeatureServiceClient;
import ru.nsu.homemoneycontrol.hmc.mapper.ReceiptMapper;
import ru.nsu.homemoneycontrol.hmc.mapper.fns.ReceiptMetainfoMapper;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;
import ru.nsu.homemoneycontrol.hmc.model.User;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;
import ru.nsu.homemoneycontrol.hmc.repository.CategoryRepository;
import ru.nsu.homemoneycontrol.hmc.repository.MainCategoryRepository;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptMetainfoRepository;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class ReceiptInteractor {


    private static final String ERROR_IN_SEND_TO_FEATURE_SERVICE = "Ошибка при отправке в сервис выделения фич";

    private final ReceiptMetainfoRepository repository;
    private final FeatureServiceClient featureServiceClient;
    private final CategoryRepository categoryRepository;

    public ResponseEntity<ReceiptDto> getReceiptMetaInfo(UUID userId, UUID receiptId) {
        ReceiptMetainfo check = getReceiptById(receiptId);

        if (!check.getAddedUser().getId().equals(userId)) {
            throw new InvalidReceiptUUID("Ошибка: чек не принадлежит пользователю");
        }
        if (check.getStatus() != CheckStatus.OK) {
            return new ResponseEntity<>(ReceiptMapper.notReadyReceiptToDto(check), HttpStatus.OK);
        }

        return new ResponseEntity<>(ReceiptMapper.receiptToDto(check), HttpStatus.OK);
    }

    @Transactional(readOnly = true)
    public ResponseEntity<List<ShortReceiptDataDto>> getReceiptShortList(UUID userId) {
        List<ReceiptMetainfo> list = getAllReceiptByUser(userId);
        return new ResponseEntity<>(ReceiptMapper.receiptListToReceiptListDto(list), HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<ReceiptDto> createReceipt(User user, CreateReceiptDto receiptDto) {
        ReceiptMetainfo receiptMetainfo = new ReceiptMetainfo();

        if (receiptDto.getIncludeDate() == null ||
                receiptDto.getPurchaseDate() == null ||
                receiptDto.getRetailPlace() == null ||
                receiptDto.getRetailAddress() == null ||
                receiptDto.getOrganizationName() == null ||
                receiptDto.getOrganizationInn() == null) {
            throw new InvalidReceiptArgument("Пропущен один из обязательных параметров");
        }

        //Установка обязательных полей
        receiptMetainfo.setAddedUser(user);
        receiptMetainfo.setIncludeDate(receiptDto.getIncludeDate());
        receiptMetainfo.setPurchaseDate(receiptDto.getPurchaseDate());
        receiptMetainfo.setRetailPlace(receiptDto.getRetailPlace());
        receiptMetainfo.setRetailAddress(receiptDto.getRetailAddress());
        receiptMetainfo.setOrganizationName(receiptDto.getOrganizationName());
        receiptMetainfo.setOrganizationInn(receiptDto.getOrganizationInn());

        receiptMetainfo.setQrCodeValue("Receipt created by user manually");
        receiptMetainfo.setStatus(CheckStatus.SUBMITTED_TO_FEATURE_SERVICE);
        //Проверка и установка необзязательных полей
        receiptMetainfo.setSellerName(receiptDto.getSellerName());
        receiptMetainfo.setSellerInn(receiptDto.getSellerInn());
        receiptMetainfo.setTotal(receiptDto.getTotal());

        List<ReceiptPosition> list = ReceiptMapper.positionsFromDto(receiptDto.getReceiptPositions(), receiptMetainfo);
        receiptMetainfo.setPositions(list);

        ReceiptMetainfo savedReceipt = repository.save(receiptMetainfo);

        sendToFeatureService(savedReceipt);

        return new ResponseEntity<>(ReceiptMapper.receiptToDto(savedReceipt), HttpStatus.OK);
    }


    private void sendToFeatureService(final ReceiptMetainfo receipt) {
        try {
            var dto = new CategorizeServiceRequestDto(receipt.getRetailPlace(), receipt.getPositions().stream().map(ReceiptPosition::getNomenclature).toList());
            ResponseEntity<CategorizeServiceResponseDto> response = featureServiceClient.sendDataToCategorizeService(dto);
            if (response.getBody() != null) {
                saveCategoryServiceResponse(response.getBody(), receipt);
            }
        } catch (Exception e) {
            log.error(ERROR_IN_SEND_TO_FEATURE_SERVICE, e);
            saveWithStatus(CheckStatus.ERROR, receipt);
            return;
        }
        saveWithStatus(CheckStatus.OK, receipt);

    }

    private void saveCategoryServiceResponse(final CategorizeServiceResponseDto responseDto,
                                             final ReceiptMetainfo receiptMetainfo) {
        if (responseDto.getResponse().isEmpty())
            return;
        for (var category : responseDto.getResponse()) {
            var receiptPosition = receiptMetainfo.getPositions().stream().filter(receiptPosition1 -> receiptPosition1.getNomenclature().equals(category.getName())).findFirst().orElseThrow(ReceiptNotFound::new);
            Category categoryForId = categoryRepository.findByCategoryName(category.getCategoryName());
            receiptPosition.setCategory(categoryForId);
        }
    }

    private void saveWithStatus(final CheckStatus status, final ReceiptMetainfo record) {
        record.setStatus(status);
        repository.save(record);
    }

    @Transactional(readOnly = true)
    public ReceiptMetainfo getReceiptById(UUID receiptId) {
        return repository.findById(receiptId).orElseThrow(ReceiptNotFound::new);
    }

    @Transactional(readOnly = true)
    public List<ReceiptMetainfo> getAllReceiptByUser(UUID userId) {
        return repository.findAllByAddedUser_Id(userId);
    }
}
