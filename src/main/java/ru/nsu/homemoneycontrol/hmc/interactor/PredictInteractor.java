package ru.nsu.homemoneycontrol.hmc.interactor;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.homemoneycontrol.hmc.dto.predict.PredictRequestDto;
import ru.nsu.homemoneycontrol.hmc.dto.predict.PredictsResponceDto;
import ru.nsu.homemoneycontrol.hmc.dto.predict.SoldPointDto;
import ru.nsu.homemoneycontrol.hmc.interactor.feign.PredictionServiceClient;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;
import ru.nsu.homemoneycontrol.hmc.repository.CategoryRepository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class PredictInteractor {

    private final CategoryRepository categoryRepository;
    private final StatisticInteractor statisticInteractor;
    private final UserInteractor userInteractor;
    private final int WEEK_COUNT = 6;

    private final PredictionServiceClient predictionServiceClient;

    public PredictsResponceDto predictItems(String userName) {

        var dto = prepareRequest(userName);
        var firstDate = Calendar.getInstance();
        var endDate = Calendar.getInstance();
        firstDate.add(Calendar.WEEK_OF_YEAR, -1);
        for (int i = 0; i < WEEK_COUNT; i++) {
            var stat = statisticInteractor.getStatisticByDate(new Date(firstDate.getTime().getTime()), new Date(endDate.getTime().getTime()));
            for (var point : dto.getData()) {
                point.getSold().add(checkContainsAndCount(stat, point.getCategoryId()));
            }
            firstDate.add(Calendar.WEEK_OF_YEAR, -1);
            endDate.add(Calendar.WEEK_OF_YEAR, -1);
        }

        var result = predictionServiceClient.sendDataToPredictItems(dto);
        return correctResult(result.getBody());
    }


    public PredictsResponceDto predictSold(String userName) {
        var dto = prepareRequest(userName);
        var firstDate = Calendar.getInstance();
        var endDate = Calendar.getInstance();
        firstDate.add(Calendar.WEEK_OF_YEAR, -1);
        for (int i = 0; i < WEEK_COUNT; i++) {
            var stat = statisticInteractor.getStatisticByDate(new Date(firstDate.getTime().getTime()), new Date(endDate.getTime().getTime()));
            for (var point : dto.getData()) {
                point.getSold().add(checkContainsAndCountSold(stat, point.getCategoryId()));
            }
            firstDate.add(Calendar.WEEK_OF_YEAR, -1);
            endDate.add(Calendar.WEEK_OF_YEAR, -1);
        }

        var result = predictionServiceClient.sendDataToPredictMoney(dto);
        return correctResult(result.getBody());
    }

    private PredictsResponceDto correctResult(PredictsResponceDto dto){

        for (var root: dto.getData()) {
            var rootResult = root.getMonthPredict().compareTo(BigDecimal.ZERO);
            if(rootResult < 0){
                root.setMonthPredict(BigDecimal.ZERO);
            }

            for(var node: root.getPredict()){
                var newList = new ArrayList<BigDecimal>();
                var nodeResult  = node.compareTo(BigDecimal.ZERO);
                if(nodeResult < 0){
                    newList.add(BigDecimal.ZERO);
                } else {
                    newList.add(node);
                }
                root.setPredict(newList);
            }
        }
        return dto;
    }


    private PredictRequestDto prepareRequest(String userName) {
        var user = userInteractor.getUser(userName);
        var dto = new PredictRequestDto();
        dto.setUserId(user.getId().toString());
        var categories = categoryRepository.findAll();
        dto.setData(new ArrayList<>());

        for (var cat : categories) {
            dto.getData().add(new SoldPointDto(cat.getId().toString(), new ArrayList<>()));
        }

        return dto;
    }

    private BigDecimal checkContainsAndCountSold(Map<Category, List<ReceiptPosition>> map, String catId) {
        AtomicReference<BigDecimal> sum = new AtomicReference<>(new BigDecimal(0));
        map.forEach((k, v) -> {
            if (k.getId().toString().equals(catId)) {
                sum.set(v.stream().map(ReceiptPosition::getSum).reduce(BigDecimal.ZERO, BigDecimal::add));
            }
        });
        return sum.get();
    }

    private BigDecimal checkContainsAndCount(Map<Category, List<ReceiptPosition>> map, String catId) {
        AtomicReference<BigDecimal> sum = new AtomicReference<>(new BigDecimal(0));
        map.forEach((k, v) -> {
            if (k.getId().toString().equals(catId)) {
                Integer count = 0;
                for (var item : v) {
                    count += item.getCount();
                }
                sum.set(new BigDecimal(count));
            }
        });
        return sum.get();
    }

}