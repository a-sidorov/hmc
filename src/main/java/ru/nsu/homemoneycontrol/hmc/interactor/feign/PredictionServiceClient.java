package ru.nsu.homemoneycontrol.hmc.interactor.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.nsu.homemoneycontrol.hmc.dto.predict.PredictRequestDto;
import ru.nsu.homemoneycontrol.hmc.dto.predict.PredictsResponceDto;
import ru.nsu.homemoneycontrol.hmc.dto.predict.ResponcePositionDto;

@FeignClient(name = "PredictionServiceClient", url = "${prediction-service.url}")
public interface PredictionServiceClient {

    @PostMapping("/predict_spent_money")
    ResponseEntity<PredictsResponceDto> sendDataToPredictMoney(@RequestBody PredictRequestDto dto);

    @PostMapping("/predict_purchased_goods")
    ResponseEntity<PredictsResponceDto> sendDataToPredictItems(@RequestBody PredictRequestDto dto);
}
