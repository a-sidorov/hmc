package ru.nsu.homemoneycontrol.hmc.interactor;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.dto.CategorizeServiceRequestDto;
import ru.nsu.homemoneycontrol.hmc.dto.CategorizeServiceResponseDto;
import ru.nsu.homemoneycontrol.hmc.dto.GetCheckResponseFromFNSDto;
import ru.nsu.homemoneycontrol.hmc.exception.BadFnsAnswerException;
import ru.nsu.homemoneycontrol.hmc.exception.ReceiptNotFound;
import ru.nsu.homemoneycontrol.hmc.interactor.feign.FeatureServiceClient;
import ru.nsu.homemoneycontrol.hmc.mapper.fns.ReceiptMetainfoMapper;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;
import ru.nsu.homemoneycontrol.hmc.repository.CategoryRepository;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptMetainfoRepository;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptPositionRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class FNSResponseInteractor {

    private static final String ERROR_IN_SEND_TO_FEATURE_SERVICE = "Ошибка при отправке в сервис выделения фич";

    private final ReceiptMetainfoRepository receiptMetainfoRepository;

    private final FeatureServiceClient featureServiceClient;

    private final ReceiptPositionRepository receiptPositionRepository;

    private final CategoryRepository categoryRepository;

    private final ReceiptMetainfoMapper receiptMetainfoMapper;


    @Transactional
    public ResponseEntity<?> saveFnsResponse(final GetCheckResponseFromFNSDto requestEntity) {
        var fnsCode = requestEntity.getStatus();
        switch (fnsCode) {
            case OK -> refreshQRCodeStatusAndSendToFeatureService(CheckStatus.OK, requestEntity);
            case ERROR -> refreshQRCodeStatusAndSendToFeatureService(CheckStatus.ERROR, requestEntity);
        }
        return ResponseEntity.ok().build();
    }

    private String getFnsCodeHeader(RequestEntity<GetCheckResponseFromFNSDto> requestEntity) {
        try {
            return requestEntity.getHeaders().get("fns-code").get(0);
        } catch (NullPointerException nullPointerException) {
            log.error("Отсутствует header 'fns-code'");
            throw new BadFnsAnswerException("Отсутствует header 'fns-code'");
        }
    }


    private void refreshQRCodeStatusAndSendToFeatureService(
            final CheckStatus checkStatus,
            final GetCheckResponseFromFNSDto requestEntity
    ) {
        Optional<ReceiptMetainfo> receiptMetainfo = receiptMetainfoRepository.findById(requestEntity.getId());
        if (receiptMetainfo.isPresent()) {
            ReceiptMetainfo receipt = receiptMetainfo.get();

            if (checkStatus == CheckStatus.ERROR) {
                saveWithStatus(CheckStatus.ERROR, receipt);
                return;
            }

            try {
                receiptMetainfoMapper.patch(receipt, receiptMetainfoMapper.map(requestEntity.getReceipt()));

                log.info("Добавлены позиции {}", receipt);

                var dto = new CategorizeServiceRequestDto(receipt.getRetailPlace(), receipt.getPositions().stream().map(ReceiptPosition::getNomenclature).toList());
                ResponseEntity<CategorizeServiceResponseDto> response = featureServiceClient.sendDataToCategorizeService(dto);
                if (response.getBody() != null) {
                    saveCategoryServiceResponse(response.getBody(), receipt);
                }
            } catch (Exception e) {
                log.error(ERROR_IN_SEND_TO_FEATURE_SERVICE, e);
                saveWithStatus(CheckStatus.ERROR, receipt);
                return;
            }
            saveWithStatus(checkStatus, receipt);
        }
    }

    private void saveWithStatus(final CheckStatus status, final ReceiptMetainfo record) {
        record.setStatus(status);
        receiptMetainfoRepository.save(record);
    }

    private void saveCategoryServiceResponse(final CategorizeServiceResponseDto responseDto,
                                             final ReceiptMetainfo receiptMetainfo) {
        if (responseDto.getResponse().isEmpty())
            return;
        for (var category : responseDto.getResponse()) {
            var receiptPosition = receiptMetainfo.getPositions().stream().filter(receiptPosition1 -> receiptPosition1.getNomenclature().equals(category.getName())).findFirst().orElseThrow(ReceiptNotFound::new);
            Category categoryForId = categoryRepository.findByCategoryName(category.getCategoryName());
            receiptPosition.setCategory(categoryForId);
        }
    }
}
