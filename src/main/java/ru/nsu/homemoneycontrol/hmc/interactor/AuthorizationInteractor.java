package ru.nsu.homemoneycontrol.hmc.interactor;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.authorization.security.jwt.JwtTokenProvider;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.exception.UserNotFound;

@Slf4j
@Service
@AllArgsConstructor
public class AuthorizationInteractor {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserInteractor userInteractor;

    /**
     * Метод логирования пользователя
     *
     * @param userDto - DTO пользователя
     * @return - DTO пользователя
     */
    @Transactional(readOnly = true)
    public ResponseEntity<String> login(final UserDto userDto) {
        String token;

        log.info("User : {} wants to authenticate", userDto.getEmail());
        String email = userDto.getEmail();
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, userDto.getPassword()));
            UserDto user = userInteractor.getUserAsDTO(email);

            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + email + " not found");
            }

            token = jwtTokenProvider.createToken(email);


            return ResponseEntity.ok(token);
        } catch (Exception userNotFound){
            throw new BadCredentialsException("Email или пароль не верны");
        }
    }

    /**
     * Метод регистрации пользователя
     *
     * @param userDto - DTO пользователя
     * @return - DTO пользователя
     */
    @Transactional
    public ResponseEntity<UserDto> registerUser(final UserDto userDto) {
        log.info("Client want to register: {}", userDto.getEmail());
        var userSaved = userInteractor.addUser(userDto);
        return ResponseEntity.ok(userSaved);
    }

}
