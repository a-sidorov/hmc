package ru.nsu.homemoneycontrol.hmc.interactor;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.dto.GetQRCodeStatusResponseDto;
import ru.nsu.homemoneycontrol.hmc.dto.fns.FnsRequestDto;
import ru.nsu.homemoneycontrol.hmc.exception.BadReceiptStatusException;
import ru.nsu.homemoneycontrol.hmc.exception.InvalidReceiptUUID;
import ru.nsu.homemoneycontrol.hmc.exception.ReceiptNotFound;
import ru.nsu.homemoneycontrol.hmc.exception.UserNotFound;
import ru.nsu.homemoneycontrol.hmc.interactor.feign.FNSClient;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptMetainfoRepository;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.UUID;

@Service
@AllArgsConstructor
public class FNSInteractor {

    private final ReceiptMetainfoRepository receiptMetainfoRepository;

    private final FNSClient fnsClient;

    private final UserRepository userRepository;

    /**
     * Метод сохранения QR кода и отправки для валидирования в сервис FNS
     *
     * @param qrCode - Строка с QR кодом
     * @return - UUID запроса в ФНС, иначе выбрасывает исключение [BadFnsAnswerException]
     */
    @Transactional
    public UUID sendQRCodeOnValidate(final String qrCode) {
        var user = userRepository.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFound::new);
        ReceiptMetainfo receiptMetainfo = getReceiptMetainfoFromQRCode(qrCode);
        receiptMetainfo.setAddedUser(user);
        receiptMetainfo.setStatus(CheckStatus.WAITING_FOR_FNS);
        ReceiptMetainfo savedReceiptMetainfo = receiptMetainfoRepository.save(receiptMetainfo);
        sendQRCodeToFnsService(receiptMetainfo);

        return savedReceiptMetainfo.getReceiptId();
    }

    /**
     * Метод проверки статуса валидирования QR кода в бд.
     *
     * @param receiptUUID - ID запроса
     * @return - ResponseEntity, с сообщением в виде строки и HTTP статусом.
     */
    @Transactional(readOnly = true)
    public GetQRCodeStatusResponseDto checkQRCodeStatus(final UUID receiptUUID) {
        var receiptMetainfo = receiptMetainfoRepository.findById(receiptUUID).orElseThrow(ReceiptNotFound::new);

        var user = userRepository.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFound::new);
        if (!receiptMetainfo.getAddedUser().equals(user))
            throw new InvalidReceiptUUID("Ошибка: чек не принадлежит пользователю");
        switch (receiptMetainfo.getStatus()) {
            case OK -> {
                return new GetQRCodeStatusResponseDto(receiptUUID, "QR код успешно прошел валидацию", CheckStatus.OK);
            }
            case ERROR -> {
                return new GetQRCodeStatusResponseDto(receiptUUID, "QR код не действителен", CheckStatus.ERROR);
            }
            case WAITING_FOR_FNS -> {
                return new GetQRCodeStatusResponseDto(receiptUUID, "QR код еще проходит валидирование", CheckStatus.WAITING_FOR_FNS);
            }
            default -> throw new BadReceiptStatusException("Статус чека не является валидируемым");
        }
    }

    @Transactional
    public UUID refreshQRCodeStatus(final UUID uuid) {
        var user = userRepository.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFound::new);
        var receiptMetainfo = receiptMetainfoRepository.findById(uuid).orElseThrow(ReceiptNotFound::new);
        if (!receiptMetainfo.getAddedUser().equals(user))
            throw new InvalidReceiptUUID("Ошибка: чек не принадлежит пользователю");
        return updateQRCode(receiptMetainfo);
    }

    private ReceiptMetainfo getReceiptMetainfoFromQRCode(final String qrCode) {
        ReceiptMetainfo receiptMetainfo = new ReceiptMetainfo();
        receiptMetainfo.setQrCodeValue(qrCode);
        receiptMetainfo.setStatus(CheckStatus.WAITING_FOR_FNS);
        receiptMetainfo.setIncludeDate(Date.valueOf(LocalDate.now()));
        return receiptMetainfo;
    }

    private void sendQRCodeToFnsService(final ReceiptMetainfo receiptMetainfo) {
        fnsClient.sendQRCodeToFNS(FnsRequestDto.builder()
                .qr(receiptMetainfo.getQrCodeValue())
                .id(receiptMetainfo.getReceiptId())
                .build());
    }

    private UUID updateQRCode(final ReceiptMetainfo receiptMetainfo) {
        // TODO: 15.11.2021 Добавить логику после появления аналитики
        return receiptMetainfoRepository.save(receiptMetainfo).getReceiptId();
    }
}
