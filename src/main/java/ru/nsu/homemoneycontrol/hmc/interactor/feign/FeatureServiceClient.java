package ru.nsu.homemoneycontrol.hmc.interactor.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import ru.nsu.homemoneycontrol.hmc.dto.CategorizeServiceRequestDto;
import ru.nsu.homemoneycontrol.hmc.dto.CategorizeServiceResponseDto;

@FeignClient(name = "FeatureServiceClient", url = "${categorize-service.url}")
public interface FeatureServiceClient {

    @PostMapping("/category")
    ResponseEntity<CategorizeServiceResponseDto> sendDataToCategorizeService(CategorizeServiceRequestDto dto);

}
