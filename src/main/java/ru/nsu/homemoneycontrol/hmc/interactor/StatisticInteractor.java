package ru.nsu.homemoneycontrol.hmc.interactor;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.dto.TotalSumByCategory;
import ru.nsu.homemoneycontrol.hmc.dto.TreeCategoryDto;
import ru.nsu.homemoneycontrol.hmc.exception.UserNotFound;
import ru.nsu.homemoneycontrol.hmc.mapper.CategoryMapper;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;
import ru.nsu.homemoneycontrol.hmc.repository.MainCategoryRepository;
import ru.nsu.homemoneycontrol.hmc.repository.ReceiptPositionRepository;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;

import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StatisticInteractor {
    private static final CheckStatus STATISTIC_STATUS = CheckStatus.OK;

    private final UserRepository userRepository;
    private final ReceiptPositionRepository receiptPositionRepository;
    private final MainCategoryRepository mainCategoryRepository;

    private List<TotalSumByCategory> countTotalPrice(Map<Category, List<ReceiptPosition>> receipts) {
        var total = new ArrayList<TotalSumByCategory>();
        for (var cat : receipts.keySet()) {
            var sum = receipts.get(cat).stream().map(ReceiptPosition::getSum).reduce(BigDecimal.ZERO, BigDecimal::add);
            total.add(TotalSumByCategory.builder()
                    .category(cat)
                    .totalSum(sum)
                    .build());
        }
        return total;
    }


    public TreeCategoryDto getStatistic(Date periodStartDate, Date periodEndDate) {
        return mapSumOnTree(getTree(), countTotalPrice(getStatisticByDate(periodStartDate,periodEndDate)));
    }

    @Transactional(readOnly = true)
    public Map<Category, List<ReceiptPosition>> getStatisticByDate(Date periodStartDate, Date periodEndDate){
        var user = userRepository.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFound::new);

        var receipts = receiptPositionRepository.findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("receipt").get("status"), STATISTIC_STATUS));
            predicates.add(criteriaBuilder.equal(root.get("receipt").get("addedUser"), user));

            if (periodStartDate != null && periodEndDate != null) {
                predicates.add(criteriaBuilder.between(root.get("receipt").get("purchaseDate"), periodStartDate, periodEndDate));
            } else if (periodEndDate == null && periodStartDate != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("receipt").get("purchaseDate"), periodStartDate));
            } else if (periodEndDate != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("receipt").get("purchaseDate"), periodEndDate));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        }).stream().collect(Collectors.groupingBy(ReceiptPosition::getCategory));

        return receipts;
    }

    private TreeCategoryDto mapSumOnTree(TreeCategoryDto tree, List<TotalSumByCategory> sums){
        for (var sum: sums) {
            CategoryMapper.mapSum(tree, sum);
        }

        return tree;
    }

    public TreeCategoryDto getTree() {
        var list = mainCategoryRepository.findAll();
        return CategoryMapper.mapAllCategories(list);
    }
}
