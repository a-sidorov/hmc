package ru.nsu.homemoneycontrol.hmc.interactor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.exception.UserAlreadyExists;
import ru.nsu.homemoneycontrol.hmc.exception.UserNotFound;
import ru.nsu.homemoneycontrol.hmc.mapper.UserDtoMapper;
import ru.nsu.homemoneycontrol.hmc.model.User;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserInteractor {
    private final UserRepository userRepository;
    private final UserDtoMapper userMapper;

    @Transactional
    public UserDto addUser(UserDto userDto) {

        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new UserAlreadyExists();
        }

        return userMapper.userToDto(
                userRepository.save(
                        userMapper.dtoToUser(userDto)
                )
        );
    }

    @Transactional(readOnly = true)
    public UserDto getUserAsDTO(final String email) {
        User user = userRepository.findUserByEmail(email).orElseThrow(UserNotFound::new);
        return userMapper.userToDto(user);
    }

    @Transactional(readOnly = true)
    public List<UserDto> getUsers() {
        List<User> users = userRepository.findAll();
        return users.stream().map(userMapper::userToDto).collect(Collectors.toList());
    }

    @Transactional
    public void deleteUser(UUID userId) {
        userRepository.deleteById(userId);
    }


    @Transactional(readOnly = true)
    public UUID getUserUUID(final String email) {
        User user = userRepository.findUserByEmail(email).orElseThrow(UserNotFound::new);
        return user.getId();
    }

    @Transactional(readOnly = true)
    public User getUser(final String email) {
        return userRepository.findUserByEmail(email).orElseThrow(UserNotFound::new);
    }

}
