package ru.nsu.homemoneycontrol.hmc.authorization.security;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nsu.homemoneycontrol.hmc.authorization.security.jwt.JwtUser;
import ru.nsu.homemoneycontrol.hmc.authorization.security.jwt.JwtUserFactory;
import ru.nsu.homemoneycontrol.hmc.exception.UserNotFound;
import ru.nsu.homemoneycontrol.hmc.repository.UserRepository;

/**
 * Сервис для аутентификации пользователя.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        var user = userRepository.findUserByEmail(userName).orElseThrow(UserNotFound::new);

        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("IN: \"loadUserByUsername\" jwtUser for user with name \"{}\" was successfully created!", userName);
        return jwtUser;
    }
}
