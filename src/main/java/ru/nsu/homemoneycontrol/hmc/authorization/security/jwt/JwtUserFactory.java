package ru.nsu.homemoneycontrol.hmc.authorization.security.jwt;

import lombok.NoArgsConstructor;
import ru.nsu.homemoneycontrol.hmc.model.User;

/**
 * Класс фабрика для создания [JwtUser]
 */
@NoArgsConstructor
public final class JwtUserFactory {
    public static JwtUser create(final User user) {
        return new JwtUser(user.getId(), user.getEmail(), user.getPassword());
    }
}
