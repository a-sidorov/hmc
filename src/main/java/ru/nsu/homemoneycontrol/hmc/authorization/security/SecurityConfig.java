package ru.nsu.homemoneycontrol.hmc.authorization.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import ru.nsu.homemoneycontrol.hmc.authorization.security.jwt.JwtConfigurer;
import ru.nsu.homemoneycontrol.hmc.authorization.security.jwt.JwtTokenProvider;

/**
 * Класс конфигурации веб секьюрити.
 */
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_ENDPOINT = "/api/users/login";
    public static final String REGISTER_ENDPOINT = "/api/users/register";
    public static final String FNS_ENDPOINT = "/api/v1/fns/qr-code-response";
    private static final String[] AUTH_WHITELIST = {"/api-docs/**", "/swagger-ui.html", "/swagger-ui/**", "/actuator/**", "/api/v1/fns/**"};
    private final JwtTokenProvider jwtTokenProvider;
    private final ObjectMapper objectMapper;


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .exceptionHandling().authenticationEntryPoint(new RestAuthenticationEntryPoint(objectMapper))
                .and()
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(LOGIN_ENDPOINT).permitAll()
                .antMatchers(REGISTER_ENDPOINT).permitAll()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers(FNS_ENDPOINT).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }
}
