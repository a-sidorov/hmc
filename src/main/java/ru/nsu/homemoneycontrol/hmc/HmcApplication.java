package ru.nsu.homemoneycontrol.hmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmcApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmcApplication.class, args);
    }

}
