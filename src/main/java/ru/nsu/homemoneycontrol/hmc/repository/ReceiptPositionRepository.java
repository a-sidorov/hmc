package ru.nsu.homemoneycontrol.hmc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;

import java.util.UUID;

@Repository
public interface ReceiptPositionRepository extends JpaRepository<ReceiptPosition, UUID>, JpaSpecificationExecutor<ReceiptPosition> {

}
