package ru.nsu.homemoneycontrol.hmc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.MainCategory;

import java.util.List;
import java.util.UUID;

@Repository
public interface MainCategoryRepository extends JpaRepository<MainCategory, Long> {
}
