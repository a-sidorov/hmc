package ru.nsu.homemoneycontrol.hmc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.homemoneycontrol.hmc.model.User;

import java.util.Optional;
import java.util.UUID;

/**
 * Репозиторий пользователя.
 */
public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findUserByEmail(String userName);

    boolean existsByEmail(String email);
}
