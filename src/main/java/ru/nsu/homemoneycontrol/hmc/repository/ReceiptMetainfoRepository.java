package ru.nsu.homemoneycontrol.hmc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReceiptMetainfoRepository extends JpaRepository<ReceiptMetainfo, UUID> {
    List<ReceiptMetainfo> findAllByStatusEquals(CheckStatus variable);
    List<ReceiptMetainfo> findAllByAddedUser_Id(UUID userId);
}
