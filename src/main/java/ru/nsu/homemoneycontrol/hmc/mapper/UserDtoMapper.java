package ru.nsu.homemoneycontrol.hmc.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
import ru.nsu.homemoneycontrol.hmc.dto.UserDto;
import ru.nsu.homemoneycontrol.hmc.model.User;

@Mapper(componentModel = "spring")
@Component
public interface UserDtoMapper {

    @Mapping(target = "password", constant = "*************")
    UserDto userToDto(User user);

    @Mapping(target = "id", ignore = true)
    User dtoToUser(UserDto userDto);
}
