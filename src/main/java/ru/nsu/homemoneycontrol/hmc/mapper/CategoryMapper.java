package ru.nsu.homemoneycontrol.hmc.mapper;

import ru.nsu.homemoneycontrol.hmc.dto.*;
import ru.nsu.homemoneycontrol.hmc.model.Category;
import ru.nsu.homemoneycontrol.hmc.model.MainCategory;

import java.util.ArrayList;
import java.util.List;


public class CategoryMapper {

    public static CategoryDto mapDto(Category category) {
        if (category == null) {
            return null;
        }
        CategoryDto dto = new CategoryDto();
        dto.setCategory(category.getCategoryName());
        dto.setId(category.getId());
        dto.setMainCategory(mapMainDto(category.getMainCategory()));
        return dto;
    }

    ;

    public static MainCategoryDto mapMainDto(MainCategory category) {
        if (category == null) {
            return null;
        }
        var dto = new MainCategoryDto();
        dto.setId(category.getId());
        dto.setMainCategoryName(category.getMainCategoryName());
        return dto;
    }

    ;

    public static TreeCategoryDto mapAllCategories(List<MainCategory> categories) {
        List<RootCategoryDto> list = new ArrayList<>();

        for (var elem : categories) {
            list.add(mapRoots(elem));
        }

        var tree = new TreeCategoryDto();
        tree.setRoot(list);

        return tree;
    }

    private static RootCategoryDto mapRoots(MainCategory mainCategory) {
        List<NodeCategoryDto> list = new ArrayList<>();

        var dto = new RootCategoryDto();

        for (var elem : mainCategory.getCategories()) {
            list.add(mapNodes(elem));
        }

        dto.setNodes(list);
        dto.setMainCategoryName(mainCategory.getMainCategoryName());
        dto.setId(mainCategory.getId());

        return dto;
    }

    private static NodeCategoryDto mapNodes(Category category) {
        var dto = new NodeCategoryDto();

        dto.setId(category.getId());
        dto.setCategory(category.getCategoryName());
        return dto;
    }

    public static void mapSum(TreeCategoryDto treeCategoryDto, TotalSumByCategory sum) {
        var root = findRoot(treeCategoryDto, sum);
        var node = findNode(root, sum);

        node.setTotalSum(sum.getTotalSum());
        root.setTotalSum(root.getTotalSum().add(node.getTotalSum()));
        treeCategoryDto.setTotalSum(treeCategoryDto.getTotalSum().add(node.getTotalSum()));
    }

    private static RootCategoryDto findRoot(TreeCategoryDto treeCategoryDto, TotalSumByCategory sum) {
        for (var roots : treeCategoryDto.getRoot()) {
            if (roots.getMainCategoryName().equals(sum.getCategory().getMainCategory().getMainCategoryName())) {
                return roots;
            }
        }
        return null;
    }

    private static NodeCategoryDto findNode(RootCategoryDto treeCategoryDto, TotalSumByCategory sum) {
        for (var nodes : treeCategoryDto.getNodes()) {
            if (nodes.getCategory().equals(sum.getCategory().getCategoryName())) {
                return nodes;
            }
        }
        return null;
    }
}
