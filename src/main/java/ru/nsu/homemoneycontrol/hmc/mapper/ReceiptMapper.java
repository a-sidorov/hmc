package ru.nsu.homemoneycontrol.hmc.mapper;


import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;
import ru.nsu.homemoneycontrol.hmc.dto.PositionDto;
import ru.nsu.homemoneycontrol.hmc.dto.ReceiptDto;
import ru.nsu.homemoneycontrol.hmc.dto.ShortReceiptDataDto;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;

import java.util.ArrayList;
import java.util.List;

@Mapper
@Component
public class ReceiptMapper {

    static public ReceiptDto receiptToDto(ReceiptMetainfo receipt) {
        return ReceiptDto.builder()
                .receiptPositions(positionsToDto(receipt.getPositions()))
                .includeDate(receipt.getIncludeDate())
                .organizationInn(receipt.getOrganizationInn())
                .organizationName(receipt.getOrganizationName())
                .purchaseDate(receipt.getPurchaseDate())
                .qrCodeValue(receipt.getQrCodeValue())
                .retailAddress(receipt.getRetailAddress())
                .retailPlace(receipt.getRetailPlace())
                .sellerInn(receipt.getSellerInn())
                .sellerName(receipt.getSellerName())
                .total(receipt.getTotal())
                .status(receipt.getStatus())
                .build();
    }

    static public ReceiptDto notReadyReceiptToDto(ReceiptMetainfo receipt) {
        return ReceiptDto.builder()
                .includeDate(receipt.getIncludeDate())
                .qrCodeValue(receipt.getQrCodeValue())
                .status(receipt.getStatus())
                .build();
    }

    static private List<PositionDto> positionsToDto(List<ReceiptPosition> pos) {
        // е использую аннотации поскольку понятия не имею как она будет работать со списком позиций
        List<PositionDto> list = new ArrayList<>();

        if (pos == null || pos.isEmpty()) {
            return list;
        }
        pos.forEach(p -> list.add(
                PositionDto.builder()
                        .nomenclature(p.getNomenclature())
                        .price(p.getPrice())
                        .count(p.getCount())
                        .sum(p.getSum())
                        .category(CategoryMapper.mapDto(p.getCategory()))
                        .build())
        );

        return list;
    }


    static public List<ShortReceiptDataDto> receiptListToReceiptListDto(List<ReceiptMetainfo> pos) {
        List<ShortReceiptDataDto> list = new ArrayList<>();

        pos.forEach(p -> {
                    var size = 0;
                    if (p.getPositions() != null) {
                        size = p.getPositions().size();
                    }
                    list.add(ShortReceiptDataDto.builder()
                            .nomenclatureCount(size)
                            .status(p.getStatus())
                            .includeDate(p.getIncludeDate())
                            .purchaseDate(p.getPurchaseDate())
                            .retailPlace(p.getRetailPlace())
                            .total(p.getTotal())
                            .id(p.getReceiptId())
                            .build()
                    );
                }
        );

        return list;
    }

    static public List<ReceiptPosition> positionsFromDto(List<PositionDto> pos, ReceiptMetainfo receiptMetainfo) {

        List<ReceiptPosition> list = new ArrayList<>();

        pos.forEach(p -> {
            ReceiptPosition dto = new ReceiptPosition();
            dto.setCategory(null);
            dto.setReceipt(receiptMetainfo);
            dto.setCount(p.getCount());
            dto.setNomenclature(p.getNomenclature());
            dto.setPrice(p.getPrice());
            dto.setSum(p.getSum());
            list.add(dto);
        });

        return list;
    }
}
