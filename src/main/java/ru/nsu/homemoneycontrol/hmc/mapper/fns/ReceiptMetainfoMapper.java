package ru.nsu.homemoneycontrol.hmc.mapper.fns;

import org.mapstruct.*;
import org.springframework.stereotype.Component;
import ru.nsu.homemoneycontrol.hmc.dto.fns.FnsData;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptMetainfo;

import java.sql.Date;

@Component
@Mapper(componentModel = "spring", uses = ReceiptPositionMapper.class)
public interface ReceiptMetainfoMapper {

    @Mapping(target = "sellerName", source = "seller.name")
    @Mapping(target = "sellerInn", source = "seller.inn")
    @Mapping(target = "organizationName", source = "organization.name")
    @Mapping(target = "organizationInn", source = "organization.inn")
    @Mapping(target = "purchaseDate", expression = "java(mapDate(fnsData))")
    @Mapping(target = "positions", source = "ticket.document.receipt.items")
    @Mapping(target = "total", source = "ticket.document.receipt.total")
    @Mapping(target = "retailPlace", source = "ticket.document.receipt.retailPlace")
    @Mapping(target = "retailAddress", source = "ticket.document.receipt.retailPlaceAddress")
    ReceiptMetainfo map(FnsData fnsData);

    default Date mapDate(FnsData fnsData) {
        if (fnsData == null) {
            return null;
        }

        var operation = fnsData.getOperation();
        if (operation == null) {
            return null;
        }

        var dateTime = operation.getPurchaseDate();
        if (dateTime != null) {
            return Date.valueOf(dateTime.toLocalDate());
        }

        return null;
    }

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void patch(@MappingTarget ReceiptMetainfo toUpdate, ReceiptMetainfo update);

    @AfterMapping
    default void patchAfterMapping(@MappingTarget ReceiptMetainfo toUpdate, ReceiptMetainfo update) {
        if (toUpdate.getPositions() != null)
            for (var position : toUpdate.getPositions()) {
                position.setReceipt(toUpdate);
            }
    }


}
