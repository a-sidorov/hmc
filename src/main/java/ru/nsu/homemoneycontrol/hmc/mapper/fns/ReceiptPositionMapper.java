package ru.nsu.homemoneycontrol.hmc.mapper.fns;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;
import ru.nsu.homemoneycontrol.hmc.dto.fns.Item;
import ru.nsu.homemoneycontrol.hmc.model.ReceiptPosition;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface ReceiptPositionMapper {

    ReceiptPosition map(Item item);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void patchList(@MappingTarget List<ReceiptPosition> toUpdate, List<ReceiptPosition> update);
}
