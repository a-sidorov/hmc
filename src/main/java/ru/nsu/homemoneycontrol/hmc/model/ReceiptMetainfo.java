package ru.nsu.homemoneycontrol.hmc.model;


import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import ru.nsu.homemoneycontrol.hmc.model.status.CheckStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "receipt_metainfo")
@RequiredArgsConstructor
public class ReceiptMetainfo {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID receiptId;

    private Date includeDate;

    private Date purchaseDate;

    private String qrCodeValue;

    private BigDecimal total;

    private String retailPlace;

    @Column(name = "retail_place_address")
    private String retailAddress;

    private String organizationName;

    private String organizationInn;

    private String sellerName;

    private String sellerInn;

    @ManyToOne
    @JoinColumn(name = "added_user_id")
    private User addedUser;

    @Enumerated
    private CheckStatus status;

    @OneToMany(mappedBy = "receipt",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            orphanRemoval = true,
            fetch = FetchType.EAGER)
    private List<ReceiptPosition> positions;
}
