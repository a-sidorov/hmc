package ru.nsu.homemoneycontrol.hmc.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "main_categories")
public class MainCategory {

    @Id
    private Long id;

    @Column(name = "category")
    private String mainCategoryName;

    @OneToMany(mappedBy = "mainCategory",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH},
            orphanRemoval = true,
            fetch = FetchType.EAGER)
    private List<Category> categories;

}
