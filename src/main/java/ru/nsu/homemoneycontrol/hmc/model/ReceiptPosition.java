package ru.nsu.homemoneycontrol.hmc.model;


import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Data
@Table(name = "receipt_position")
public class ReceiptPosition {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "receipt_id")
    private ReceiptMetainfo receipt;

    @Column(name = "nomenclature")
    private String nomenclature;

    private BigDecimal price;

    private Integer count;

    private BigDecimal sum;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
