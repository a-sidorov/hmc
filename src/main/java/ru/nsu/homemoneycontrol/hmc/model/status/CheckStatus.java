package ru.nsu.homemoneycontrol.hmc.model.status;

/**
 * Класс перечисление статуса чека
 */
public enum CheckStatus {
    ERROR,
    WAITING_FOR_FNS,
    OK,
    SUBMITTED_TO_FEATURE_SERVICE,
}
