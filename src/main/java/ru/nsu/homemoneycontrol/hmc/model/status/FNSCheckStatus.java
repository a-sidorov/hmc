package ru.nsu.homemoneycontrol.hmc.model.status;

/**
 * Класс перечисление статуса чека для ФНС
 */
public enum FNSCheckStatus {
    ERROR,
    OK

}
