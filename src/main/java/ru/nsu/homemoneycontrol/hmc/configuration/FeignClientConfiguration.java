package ru.nsu.homemoneycontrol.hmc.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"ru.nsu.homemoneycontrol.hmc.interactor.feign"})
public class FeignClientConfiguration {

    // TODO: 23.09.2021 настроить после секюра
//    @Bean
//    public RequestInterceptor requestInterceptor() {
//
//        return requestTemplate -> {
//            var cred = (KeycloakSecurityContext) SecurityContextHolder.getContext().getAuthentication().getCredentials();
//            var auth = cred.getTokenString();
//            var type = cred.getToken().getType();
//            requestTemplate.header("Authorization", String.format("%s %s", type, auth));
//            requestTemplate.header("Accept", ContentType.APPLICATION_JSON.getMimeType());
//        };
//    }

}
