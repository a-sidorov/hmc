﻿CREATE EXTENSION "uuid-ossp";

CREATE TABLE main_categories
(
    id       int primary key,
    category varchar(1000) not null
);

alter table categories add  main_category_id int;
alter table categories add constraint fk_main_category foreign key (main_category_id) references main_categories (id);


INSERT INTO main_categories (id,category) VALUES (1,'Продукты');
INSERT INTO main_categories (id,category) VALUES (2,'Для себя');
INSERT INTO main_categories (id,category) VALUES (3,'Для дома');
INSERT INTO main_categories (id,category) VALUES (4,'Неизвестно');

INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Мясо и птица',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Колбасные изделия',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Фрукты и овощи',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Кондитерские изделия',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Чай, кофе, какао',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Бакалея',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Полуфабрикаты',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Молоко, сыр, яйцо',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Рыба и морепродукты',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Готовая еда',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Безалкогольные напитки',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Алкогольные напитки',1);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Выпечка',1);

INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Здоровье',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Красота',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Средства личной гигиены',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Автотовары',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Одежда и обувь',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Канцелярия и печатная продукция',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Цветы и подарки',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Табачная продукция',2);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Кафе и рестораны',2);

INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Бытовая химия',3);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Спорт и активный отдых',3);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Товары для животных',3);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Бытовая техника и электроника',3);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Товары для детей',3);
INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Все для дома (посуда) и сада',3);

INSERT INTO categories (id, category, main_category_id) VALUES (uuid_generate_v1(), 'Неизвестно',4);

