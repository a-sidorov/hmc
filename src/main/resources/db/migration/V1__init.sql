CREATE TABLE users
(
    id       uuid primary key,
    email    varchar(50)   not null unique,
    password varchar(1000) not null
);

CREATE TABLE categories
(
    id               uuid primary key,
    category         varchar(1000) not null
);

CREATE TABLE receipt_metainfo
(
    receipt_id           uuid primary key,
    include_date         date    not null default now(),
    purchase_date        date,
    qr_code_value        text    not null,
    total                decimal,
    retail_place         text,
    retail_place_address text,
    organization_name    text,
    organization_inn     varchar(20),
    seller_name          text,
    seller_inn           varchar(20),
    added_user_id        uuid,
    status               integer not null,


    constraint fk_user foreign key (added_user_id) references users (id)
);

CREATE TABLE receipt_position
(
    id           uuid primary key,
    receipt_id   uuid,
    nomenclature varchar(1000) not null,
    price        decimal       not null,
    count        bigint        not null,
    sum          decimal       not null,
    category_id  uuid          not null,

    constraint fk_category foreign key (category_id) references categories (id),
    constraint fk_receipt foreign key (receipt_id) references receipt_metainfo (receipt_id)
);

